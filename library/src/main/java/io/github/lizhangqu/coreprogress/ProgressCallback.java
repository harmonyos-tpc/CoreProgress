/**
 * Copyright 2017 区长
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.lizhangqu.coreprogress;

/**
 * Stream read/write progress
 */
interface ProgressCallback {
    /**
     * The progress changes. If the values of numBytes, totalBytes, and percent are -1,
     * the total size cannot be obtained.
     *
     * @param numBytes   Read/Write Size
     * @param totalBytes Total Size
     * @param percent    Percentage
     */
    void onProgressChanged(long numBytes, long totalBytes, float percent);
}
