/**
 * Copyright 2017 区长
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.lizhangqu.coreprogress;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * Progress callback request body/response body wrapper class.
 */
public class ProgressHelper {
    /**
     * Pack the request body with progress.
     *
     * @param requestBody      Request body to be packed
     * @param progressListener Progress callback listening
     * @return Request body with progress. This request body is used for requests.
     */
    public static RequestBody withProgress(RequestBody requestBody, ProgressListener progressListener) {
        if (requestBody == null) {
            throw new IllegalArgumentException("requestBody == null");
        }
        if (progressListener == null) {
            throw new IllegalArgumentException("progressListener == null");
        }
        return new ProgressRequestBody(requestBody, progressListener);
    }

    /**
     * Packing the request body with the response body with progress
     *
     * @param responseBody     Response body to be packaged
     * @param progressListener Progress callback listening
     * @return Response body with progress，This response body is used to read response data.
     */
    public static ProgressResponseBody withProgress(ResponseBody responseBody, ProgressListener progressListener) {
        if (responseBody == null) {
            throw new IllegalArgumentException("responseBody == null");
        }
        if (progressListener == null) {
            throw new IllegalArgumentException("progressListener == null");
        }
        return new ProgressResponseBody(responseBody, progressListener);
    }
}
